/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.controller;

import java.util.Arrays;
import java.util.List;
import mas.entity.Wine;
import mas.service.WineService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.mockito.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;


public class TestWineController {
    

    
    private MockMvc mockMvc;
    @Mock
    private WineService wineService;
    @InjectMocks
    private WineController wineController ;
   
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        
        wineController = new WineController();
        mockMvc=MockMvcBuilders
                .standaloneSetup(wineController)
                .build();
    }
     /**
     * Test of getWineList method, of class WineController.
     */
    @Test
    public void testTestMethod()throws Exception{
         
        mockMvc.perform(
                MockMvcRequestBuilders.get("/wines/test")
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("test1"));
    }
    @Test
    public void testGetWineList() throws Exception{
        Wine wine1 = new Wine("Vdova Klicko", "Shabo");
        Wine wine2 = new Wine("Kagor","OVZ");
        Wine wine3 = new Wine("Portwine 777", "ZZW");
        List<Wine> wines=Arrays.asList(wine1,wine2,wine3);
        when(wineService.getAllWines()).thenReturn(wines);
//        mockMvc.perform(MockMvcRequestBuilders.get("/wines/getAll"))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                ;
//        mockMvc.perform(MockMvcRequestBuilders.get("/wines/getAll"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                               
//                ;


    }

    /**
     * Test of getWineById method, of class WineController.
     */
    @Test
    public void testGetWineById() {

//        fail("The test case is a prototype.");
    }
    
}
