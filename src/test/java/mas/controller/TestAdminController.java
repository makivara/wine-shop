/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.controller;

import java.util.ArrayList;
import java.util.List;
import mas.entity.Wine;
import org.junit.Test;

/**
 *
 * @author Makivchuk
 */

public class TestAdminController {
    @Test
    public void shouldShowWineList() throws Exception{
        List<Wine> wineList = createWineList(20);        
    }
    
    public List<Wine> createWineList(long count){
        List<Wine> listOfWine = new ArrayList<Wine>();
        for(long i=0;i<count;i++){
            Wine wine = new Wine();
            wine.setId(i);           
            wine.setName("Wine"+i);
            wine.setTitle("Title" +i);
            listOfWine.add(wine);
        }
        return listOfWine;
    }
}
