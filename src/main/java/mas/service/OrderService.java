/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.service;

import java.util.List;
import mas.entity.Order;


public interface OrderService {
    List<Order> getAllOrders();
    List<Order> getActiveOrders();
    Order getOrderById(Long id);
    void insert(Order order);
    void update(Order order);
    void delete(Order order);
}
