/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.service;

import java.util.List;
import mas.entity.Wine;
import org.springframework.stereotype.Service;

/**
 *
 * @author Makivchuk
 */

public interface WineService {
    List<Wine> getAllWines();
    Wine getWineById(Long id);
    Wine insertWine(Wine wine);
    Wine updateWine(Wine wine);
    void deleteWine(Wine wine);
}
