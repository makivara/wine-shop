/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.service;

import java.util.List;
import mas.entity.Wine;
import mas.repository.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Makivchuk
 */
@Service("wineService")
@Transactional
public class WineServiceImpl implements WineService{
    
    private WineRepository wineRepository;
    
    @Autowired
    public void WineServiceImpl(WineRepository wineRepository){
        this.wineRepository=wineRepository;
    }
    @Override
    public List<Wine> getAllWines() {
       return wineRepository.getAllWines();
    }

    @Override
    public Wine getWineById(Long id) {
        return wineRepository.getWineById(id);
    }

    @Override
    public Wine insertWine(Wine wine) {
        return wineRepository.insertWine(wine);
    }

    @Override
    public Wine updateWine(Wine wine) {
        return wineRepository.updateWine(wine); 
    }

    @Override
    public void deleteWine(Wine wine) {
        wineRepository.deleteWine(wine);
    }
    
}
