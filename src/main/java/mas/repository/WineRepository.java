/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.repository;

import java.util.List;
import mas.entity.Wine;

/**
 *
 * @author Makivchuk
 */
public interface WineRepository {
    List<Wine> getAllWines();
    Wine getWineById(Long id);
    Wine insertWine(Wine wine);
    Wine updateWine(Wine wine);
    void deleteWine(Wine wine);
     
}
