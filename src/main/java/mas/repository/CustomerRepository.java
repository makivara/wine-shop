/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.repository;

import mas.entity.Customer;

/**
 *
 * @author Денис
 */
public interface CustomerRepository {
    
    void insertCustomer(Customer customer);
    void updateCustomer(Customer customer);
    void deleteCustomer(Customer customer);
}
