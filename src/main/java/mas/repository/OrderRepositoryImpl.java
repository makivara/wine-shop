/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.repository;

import java.lang.annotation.Repeatable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mas.entity.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class OrderRepositoryImpl implements OrderRepository{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public List<Order> getAllOrders() {
        String sql = "SELECT o FROM Order o";        
        return entityManager.createQuery(sql).getResultList();
    }

    @Override
    public List<Order> getActiveOrders() {
        String sql = "SELECT o FROM Order o WHERE o.status=:status";
        Query query=entityManager.createQuery(sql, Order.class);
        query.setParameter("status", "ACTIVE");
        try{
            return query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Order getOrderById(Long id) {
        String sql="SELECT o FROM Order o WHERE o.id=:id";
        Query query = entityManager.createQuery(sql, Order.class);
        query.setParameter("id", id);
        try{
            return (Order)query.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insert(Order order) {
       entityManager.persist(order);
    }

    @Override
    public void update(Order order) {
        entityManager.merge(order);
    }

    @Override
    public void delete(Order order) {
        entityManager.remove(order);
    }
    
}
