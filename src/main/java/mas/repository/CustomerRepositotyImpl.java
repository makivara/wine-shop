/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mas.entity.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CustomerRepositotyImpl implements CustomerRepository{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void insertCustomer(Customer customer) {
        entityManager.persist(customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        entityManager.merge(customer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        entityManager.remove(customer);
    }
    
}
