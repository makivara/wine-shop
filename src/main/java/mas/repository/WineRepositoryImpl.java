/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mas.entity.Wine;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Makivchuk
 */
@Repository("wineDAO")
@Transactional
public class WineRepositoryImpl implements WineRepository{
    
    @PersistenceContext
    public EntityManager entityManager;


    public WineRepositoryImpl() {
    
    }
    
    
    
    @Override
    public List<Wine> getAllWines() {
        String sql ="SELECT w FROM Wine w";
        Query query = entityManager.createQuery(sql);
        List<Wine> wineList = query.getResultList();
        return wineList;


    }

    @Override
    public Wine getWineById(Long id) {
        String sql = "select w from Wine w where w.id=:id";
        Query query = entityManager.createQuery(sql);
        query.setParameter("id", id);
        Wine wine;
        try{
            wine =(Wine) query.getSingleResult();
            System.out.print(wine); 
            return wine;
        }catch(Exception e){
            System.out.print("Exception "+ e);
        }
        return null;
    }

    @Override
    public Wine insertWine(Wine wine) {
        entityManager.persist(wine);
        return wine;
    }

    @Override
    public Wine updateWine(Wine wine) {
        entityManager.merge(wine);
        return wine;
    }

    @Override
    public void deleteWine(Wine wine) {
        entityManager.remove(wine);
    }

    
    
    
    
}
