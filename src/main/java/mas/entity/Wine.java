/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WINE")
public class Wine implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="WINE_ID")
    private Long id;
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name="TITLE")
    private String title;
    
    @Column(name="PRICE")
    private Integer price;
    
  
    public Wine(){
    }
    public Wine(String name, String title){
        this.name=name;
        this.title=title;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }


    
        
    @Override
    public String toString() {
        return "Wine{" + "id=" + id + ", name=" + name + ", title=" + title + ", price=" + price +  '}';
    }
    
   
    
}
