/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.controller;

import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Makivchuk
 */
@Controller
@RequestMapping(value="/")
public class MyController {
    
    @RequestMapping(method=RequestMethod.GET)
    public String getStartedPage(HttpServletResponse response){
//        return "static";
        return "forward:/static/static.html";
    }
    @RequestMapping(value="/administration/", method=RequestMethod.GET)
    public String home(){
//        return "adminpage";
        return "forward:/static/adminpage.html";
    }
}
