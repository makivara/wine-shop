/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.controller;

import java.util.List;
import mas.entity.Order;
import mas.entity.Wine;
import mas.service.OrderService;
import mas.service.WineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Makivchuk
 */
@RestController
@RequestMapping(value="/administration")
public class AdminController {
    private WineService wineService;
    private OrderService orderService;

    @Autowired
    public AdminController(WineService wineService, OrderService orderService){
        this.wineService=wineService;
        this.orderService=orderService;
    }
    
    
    @RequestMapping(value="/wines",method = RequestMethod.GET, headers = "Accept=application/json")
    public List<Wine> getWines(){
        
        return wineService.getAllWines();
    }
    @RequestMapping(value="/wines/wine{id}",method = RequestMethod.GET, headers = "Accept=application/json")
    public Wine getWine(@PathVariable Long id){
        return wineService.getWineById(id);
    }
    @RequestMapping(value = "/wines/addWine", method = RequestMethod.POST, headers = "Accept=application/json")
    public String addWine(@RequestBody Wine wine){
        wineService.insertWine(wine);
        
        return "Added";
    }
    @RequestMapping(value = "/wines/wine{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity updateWine(@PathVariable Long id, @RequestBody Wine wine){
        wineService.updateWine(wine);
//        return new ResponseEntity(HttpStatus.OK);
        System.out.println(ResponseEntity.status(HttpStatus.OK).body(null));
        return  ResponseEntity.status(HttpStatus.OK).body(null);
    }
//    @RequestMapping(value = "/wines/wine{id}", method = RequestMethod.DELETE)
//    public ResponseEntity deleteWine(@PathVariable Long id){
//        
//        return new ResponseEntity(HttpStatus.OK);
//    }
    
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public List<Order> getOrders(){
        return orderService.getAllOrders();
    }
    

    

   
}
