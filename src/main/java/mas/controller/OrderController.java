
package mas.controller;

import mas.entity.Order;
import mas.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value="/orders/")
@RestController
public class OrderController {
    
    private OrderService orderService;
    @Autowired
    public void setOrderService(OrderService  orderService) {
        this.orderService = orderService;
    }
    
    
    
    @RequestMapping(value="/addOrder", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<String> addOrder(@RequestBody Order order){
        System.out.println(order);
        orderService.insert(order);
        return  new ResponseEntity<>("Sucsess", HttpStatus.OK);
    }
    
}
