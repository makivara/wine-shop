/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.controller;


import java.util.List;
import mas.entity.Wine;
import mas.service.WineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/wines")
public class WineController {
    
    private WineService wineService;
    @Autowired
    public void setWineService(WineService wineService){
        this.wineService=wineService;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String getTest(){
    return "test1";
    }
    @RequestMapping(value="/getAll", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<Wine> getWineList(){
       return wineService.getAllWines();
       
    }
    
    @RequestMapping(value="/getwineby{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<Wine> getWineById(@PathVariable Long id){
        Wine wine = wineService.getWineById(id);
        if(wine==null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(wine, HttpStatus.OK);
    }
    
        
}
