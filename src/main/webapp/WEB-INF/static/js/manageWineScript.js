var adminUrl="/administration";

function ConvertFormToJson(form){
    var array = jQuery(form).serializeArray();
    var json ={};
    jQuery.each(array, function(){
        json[this.name]=this.value || '';
    });
    return json;
};

$(document).ready(function(){  
    
    $('#addWineSubmit').click(function(){
        var postWineUrl= adminUrl+'/wines/addWine';
        var form  = $('#addWineForm');
        var json = JSON.stringify(ConvertFormToJson(form));
        $.ajax({
            type: "POST",
            url: postWineUrl,
            data: json,
            success: function(){
            console.log('sucsses');
        },
            dataType: "json",
            contentType : "application/json"
        });
        alert('Wine Added');
        $('#addWineClose').click();
    });
    
    $("#updateWineSubmit").click(function (){
        var id= $('#updateWineForm').children('input[name="id"]').val();
        var updateWineUrl= adminUrl+'/wines/wine'+id;
        var form = $('#updateWineForm');
        var json = JSON.stringify(ConvertFormToJson(form));
        console.log(json);
        console.log('id :' + id);
        console.log('updateWineUrl : ' + updateWineUrl);
        $.ajax({
            method: 'PUT',
            url: updateWineUrl,
            data: json,
//            dataType: 'json',
            contentType: 'application/json'
        })
        .done(function (){
            console.log('sucses');
            
        })
        .fail(function(){
            console.log('fail');
//           $('#getAllWines').click();
        })
        .always(function(){
            $('#getAllWines').click();
        });
        

    });
    
    $('#submitSearchWineById').click(function (){
        var getWineByIdUrl = adminUrl+ '/wines/wine' + $("#searchWineId").val();
        var result;
        $('#wine').empty();
        $.get(getWineByIdUrl,function(data){
            $('#wine').append("id = " + data.id+ ", name = "+ data.name+ ", title = " + data.title+ ", price = "+ data.price+ ";");
            console.log(data);
        });            
    });    
    
    $('#getAllWines').click(function(){
        $('#wineList').empty();
        var getAllWinesUrl = adminUrl+ '/wines';
        $.get(getAllWinesUrl, function (data){

            var response=data;
            $(function(){
                var columnKeys = [];
                for (var i = 0; i < response.length; i++) {
                    for (var key in response[i]) {
                        if (columnKeys.indexOf(key) === -1) {
                            columnKeys.push(key);
                        }
                    }
                }

                var  $table = $('<table></table>');
                var thead=$('<thead></thead>');
                columnKeys.forEach(function (key){
                    thead.append($('<th>').text(key));
                });
                $table.append(thead);
                $.each(response, function(i, item){
                    var link =   "<a href='#' id='wine"+ item.id+ "'></a>";
                    var $tr = $('<tr>').append(
                        $('<td>').append($(link).text(item.id).click(
                            function(){
                                console.log(' wine id '+ item.id + ' is clicked');
                                $('#updateWine').modal('show');
                                $.get(adminUrl+ '/wines/wine' +item.id, function (data){
                                    $('#updateWineForm').children('input[name="id"]').val(data.id);
                                    $('#updateWineForm').children('input[name="title"]').val(data.title);
                                    $('#updateWineForm').children('input[name="name"]').val(data.name);
                                    $('#updateWineForm').children('input[name="price"]').val(data.price);
                                });
                            }
                        )),
                        $('<td>').text(item.name),
                        $('<td>').text(item.title),
                        $('<td>').text(item.price)
                    );
                    $table.append($tr);
                });                
                $($table).addClass('table');
                $('#wineList').append($($table));
            });
        });
    });
        
});

//var wine;
//var xmlhttp = new XMLHttpRequest();
//
//function getAllWines(){
//    var wines;
//    var getUrl = adminUrl+ '/wines';
//    console.log(getUrl);
//    xmlhttp.open('GET', getUrl, true);
//    xmlhttp.send();
//    xmlhttp.onreadystatechange = function() {
//        if(xmlhttp.readyState == 4 && xmlhttp.status==200){
//            var text =xmlhttp.responseText;
//            wines = JSON.parse(text);
//            var columnKeys = [];
//            for (var i = 0; i < wines.length; i++) {
//                for (var key in wines[i]) {
//                    if (columnKeys.indexOf(key) === -1) {
//                        columnKeys.push(key);
//                    }
//                }
//            }
//            var table = document.createElement("table");
//            var tr = table.insertRow(-1);                   // TABLE ROW.
//
//            for (var i = 0; i < columnKeys.length; i++) {
//                var th = document.createElement("th");      // TABLE HEADER.
//                th.innerHTML = columnKeys[i];
//                tr.appendChild(th);
//            }            
//            for (var i = 0; i < wines.length; i++) {
//                tr = table.insertRow(-1);
//                for (var j = 0; j < columnKeys.length; j++) {
//                    var tabCell = tr.insertCell(-1);
//                    tabCell.innerHTML = wines[i][columnKeys[j]];
//                }
//            }            
//            var divConteiner = document.getElementById('wineList');
//            divConteiner.innerHTML="";
//            divConteiner.appendChild(table);
//            console.log(document.baseURI);
//        }else{
//
//        }    
//    };
//};